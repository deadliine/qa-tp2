package ca.qc.claurendeau.storage;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;
import org.apache.commons.collections4.queue.CircularFifoQueue;


public class Buffer
{
    CircularFifoQueue<Element> queue;

    public Buffer(int capacity) {
        this.queue = new CircularFifoQueue<Element>(capacity);
    }

    public String toString() {
        return queue.toString();
    }

    public int capacity() {
        return queue.maxSize();
    }

    public int getCurrentLoad() {
        return queue.size();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public boolean isFull() {
        return queue.isAtFullCapacity();
    }

    public synchronized void addElement(Element element) throws BufferFullException{
        if(queue.isAtFullCapacity())
            throw new BufferFullException("Buffer is full");

        queue.add(element);
    }

    public synchronized Element removeElement() throws BufferEmptyException{
        if (queue.peek() == null)
            throw new BufferEmptyException("Buffer is empty");
        return queue.poll();
    }
}
