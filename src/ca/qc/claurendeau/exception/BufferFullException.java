package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {

    private BufferFullException() {}

    public BufferFullException(String message) {
        super(message);
    }
}
