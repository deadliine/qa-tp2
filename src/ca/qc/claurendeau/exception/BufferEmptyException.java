package ca.qc.claurendeau.exception;

public class BufferEmptyException extends Exception {

    private BufferEmptyException() {}

    public BufferEmptyException(String message) {
        super(message);
    }
}
