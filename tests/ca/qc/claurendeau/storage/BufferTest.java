package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;
import org.junit.Test;

public class BufferTest {

    @Test
    public void testBuildSuccess() {
        assertTrue(true);
    }

    @Test
    public void testCapacity(){
        int capacite = 10;
        Buffer buffer = new Buffer(capacite);
        assertTrue("La capacite ne correspond pas", capacite == buffer.capacity());
    }

    @Test(expected = BufferFullException.class)
    public void testAddElement() throws Exception{
        int capacite = 3;
        Buffer buffer = new Buffer(capacite);
        buffer.addElement(new Element(1));
        buffer.addElement(new Element(2));
        buffer.addElement(new Element(3));
        buffer.addElement(new Element(4));
        buffer.addElement(new Element(5));
    }

    @Test
    public void testGetCurrentLoad()throws Exception{
        int capacite = 10;
        Buffer buffer = new Buffer(capacite);
        buffer.addElement(new Element(1));
        buffer.addElement(new Element(2));
        buffer.addElement(new Element(3));
        buffer.addElement(new Element(4));
        buffer.addElement(new Element(5));
        assertTrue("le nombre d'element present ne correspond pas", buffer.getCurrentLoad() == 5);
    }

    @Test
    public void testIsEmpty() throws Exception{
        int capacite = 10;
        Buffer buffer = new Buffer(capacite);
        assertTrue("le buffer devrait etre vide", buffer.isEmpty());
        buffer.addElement(new Element(1));
        buffer.addElement(new Element(2));
        buffer.addElement(new Element(3));
        buffer.addElement(new Element(4));
        buffer.addElement(new Element(5));
        assertFalse("le buffer devrait etre vide", buffer.isEmpty());
    }

    @Test
    public void testIsFull() throws Exception{
        int capacite = 5;
        Buffer buffer = new Buffer(capacite);
        assertFalse("le buffer ne devrait pas etre plein", buffer.isFull());
        buffer.addElement(new Element(1));
        buffer.addElement(new Element(2));
        buffer.addElement(new Element(3));
        buffer.addElement(new Element(4));
        buffer.addElement(new Element(5));
        assertTrue("le buffer devrait etre plein", buffer.isFull());
    }

    @Test(expected = BufferEmptyException.class)
    public void testRemoveElement() throws Exception{
        int capacite = 10;
        Buffer buffer = new Buffer(capacite);
        buffer.addElement(new Element(1));
        buffer.addElement(new Element(2));
        buffer.addElement(new Element(3));

        buffer.removeElement();
        buffer.removeElement();
        buffer.removeElement();
        buffer.removeElement();

    }

}


